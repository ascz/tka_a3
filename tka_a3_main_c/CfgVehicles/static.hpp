class RDS_ZU23_AAF;
class RDS_Igla_AA_pod_AAF;
class RDS_KORD_AAF;
class RDS_KORD_high_AAF;
class RDS_AGS_AAF;
class RDS_Metis_AAF;
class RDS_2b14_82mm_AAF;
class RDS_D30_AAF;
class TKA_A3_ZU23: RDS_ZU23_AAF
{
	author = "TKA_A3";
    scope=2;
    scopeCurator=2;
    faction="TKA_A3";
    side=0;
    crew = "B_TKA_A3_Soldier";
	typicalCargo[]={"B_TKA_A3_Soldier"};
};

class TKA_A3_KORD: RDS_KORD_AAF
{
	author = "TKA_A3";
    scope=2;
    scopeCurator=2;
    faction="TKA_A3";
    crew = "B_TKA_A3_Soldier";
	typicalCargo[]={"B_TKA_A3_Soldier"};
    side=0;
};
class TKA_A3_KORD_high: RDS_KORD_high_AAF
{
	author = "TKA_A3";
    scope=2;
    scopeCurator=2;
    faction="TKA_A3";
    crew = "B_TKA_A3_Soldier";
	typicalCargo[]={"B_TKA_A3_Soldier"};
    side=0;
};

class TKA_A3_AGS:RDS_AGS_AAF
{
	author = "TKA_A3";
    scope = 2;
    scopeCurator=2;
    faction="TKA_A3";
    side=0;
    crew = "B_TKA_A3_Soldier";
	typicalCargo[]={"B_TKA_A3_Soldier"};
};
class TKA_A3_Metis: RDS_Metis_AAF
{
	author = "TKA_A3";
    scope = 2;
    scopeCurator=2;
    faction="TKA_A3";
    side=0;
    crew = "B_TKA_A3_Soldier";
	typicalCargo[]={"B_TKA_A3_Soldier"};
};
class TKA_A3_Igla_AA_pod : RDS_Igla_AA_pod_AAF
{
	author = "TKA_A3";
    scope = 2;
    scopeCurator=2;
    faction="TKA_A3";
    side=0;
    crew = "B_TKA_A3_Soldier";
	typicalCargo[]={"B_TKA_A3_Soldier"};
};
class TKA_A3_2b14_82mm: RDS_2b14_82mm_AAF
{
	author = "TKA_A3";
    scope=2;
    scopeCurator=2;
    faction="TKA_A3";
    side=0;
    crew = "B_TKA_A3_Soldier";
	typicalCargo[]={"B_TKA_A3_Soldier"};
};
class TKA_A3_D30: RDS_D30_AAF
{
	author = "TKA_A3";
    scope=2;
    scopeCurator=2;
    faction="TKA_A3";
    side=0;
    crew = "B_TKA_A3_Soldier";
	typicalCargo[]={"B_TKA_A3_Soldier"};
};