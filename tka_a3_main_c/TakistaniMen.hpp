class CfgWorlds
{
    class GenericNames
    {
        class TakistaniMen
        {
            class FirstNames
            {
                Abdul_Aziz = "Abdul-Aziz";
                Abdulla_h = "Abdullah";
                Azim = "Azim";
                Akbar = "Akbar";
                Khairulla_h = "Khairullah";
                Sami = "Sami";
                Sadat = "Sadat";
                Sibhatulla_h = "Sibhatullah";
                Hasan = "Hasan";
                Hussein = "Hussein";
                Habib = "Habib";
                Umar = "Umar";
                Hasib = "Hasib";
                Abdul_Latif = "Abdul-Latif";
                Ali = "Ali";
                Karim = "Karim";
                Kabir = "Kabir";
                Luqman = "Luqman";
                Mahmood = "Mahmood";
                Rahim = "Rahim";
                Jamal = "Jamal";
                Latif = "Latif";
                Adil = "Adil";
                Farid = "Farid";
                Jajil = "Jajil";
                Khalil = "Khalil";
                Mustafa = "Mustafa";
                Qasim = "Qasim";
                Hafiz = "Hafiz";
                Abbas = "Abbas";
                Abdul_Basir = "Abdul-Basir";
                Abdul_Mussawir = "Abdul-Mussawir";
                Qadeer = "Qadeer";
                Abdul_Qa_dir = "Abdul-Qadir";
                Abdul_Wahhab = "Abdul-Wahhab";
                Majeed = "Majeed";
                Amir = "Amir";
                Arif = "Arif";
                Aslan = "Aslan";
                Basharat = "Basharat";
                Bashir = "Bashir";
                Ehsan = "Ehsan";
                Faisal = "Faisal";
                Haikal = "Haikal";
                Idris = "Idris";
                Isma_i_l = "Ismail";
                Jabr = "Jabr";
                Jafar = "Jafar";
                Mufid = "Mufid";
            };
            class LastNames
            {
                Saikal = "Saikal";
                Ahmed_Khan = "Ahmed-Khan";
                Kakar = "Kakar";
                Ra_tebza_d = "Ratebzad";
                Tanwir = "Tanwir";
                Siddiqi = "Siddiqi";
                Wardak = "Wardak";
                Fahim = "Fahim";
                Rahimi = "Rahimi";
                Ajani = "Ajani";
                Takhtar = "Takhtar";
                Bahadur = "Bahadur";
                Ama_ni = "Amani";
                Nazari = "Nazari";
                Muhtaram = "Muhtaram";
                Khalili = "Khalili";
                Spanta = "Spanta";
                Zakhilwal = "Zakhilwal";
                Khoram = "Khoram";
                Faruqi = "Faruqi";
                Sangeen = "Sangeen";
                Adel = "Adel";
                Anwari = "Anwari";
                Amin = "Amin";
                Masood = "Masood";
                Gailani = "Gailani";
                Ghafurzai = "Ghafurzai";
                Jalali = "Jalali";
                Yusufzai = "Yusufzai";
                Aybak = "Aybak";
                Kohzad = "Kohzad";
                Haidari = "Haidari";
                Habibzai = "Habibzai";
                Zamani = "Zamani";
                Mohammadi = "Mohammadi";
                Habibi = "Habibi";
                Yousuf = "Yousuf";
                Khusraw = "Khusraw";
                Bakhtari = "Bakhtari";
                Jawadi = "Jawadi";
                Karizi = "Karizi";
                Shah = "Shah";
                Ahmed_Jan = "Ahmed-Jan";
                Khara = "Khara";
                Hakimi = "Hakimi";
                Noori = "Noori";
                Sabet = "Sabet";
                Zahor = "Zahor";
                Kushan = "Kushan";
            };
        };
        class AzizMen
        {
            class FirstNames
            {
                Muhammad_Rahim = "Muhammad Rahim";
            };
            class LastNames
            {
                Aziz = "Aziz";
            };
        };
    };
};