class CfgFaces
{
	class Default
	{
		class Custom;
	};
	class Man_A3: Default
	{
		class Default;
	    class TKA_A3_Aziz: Default
		{
			displayname = "Aziz";
			texture = "\tka_a3\tka_a3_main\data\Heads\Aziz_co.paa";
			head = "GreekHead_A3";
			identityTypes[] = {"Head_Aziz"};
			author = "TKA_A3";
			material = "\tka_a3\tka_a3_main\data\Heads\Aziz.rvmat";
			materialWounded1 = "A3\Characters_F\Heads\Data\m_persian_03_injury.rvmat";
			materialWounded2 = "A3\Characters_F\Heads\Data\m_persian_03_injury.rvmat";
			textureHL = "\A3\Characters_F\Heads\Data\hl_black_bald_co.paa";
			materialHL = "\A3\Characters_F\Heads\Data\hl_White_old.rvmat";
			textureHL2 = "\A3\Characters_F\Heads\Data\hl_black_bald_co.paa";
			materialHL2 = "\A3\Characters_F\Heads\Data\hl_White_old.rvmat";
		};
	};
};