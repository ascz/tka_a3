class CfgPatches
{
	class tka_a3_vehicles
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Armor_F","A3_Soft_F","rhs_c_a2port_car","RDS_StaticWeapons_Core"};
	};
};
class CfgMagazines {
    class CA_LauncherMagazine;
    class RDS_PG9_AT: CA_LauncherMagazine
    {
        delete model;
    };
    class RDS_OG9_HE: CA_LauncherMagazine
    {
        delete model;
    };
};
class CfgMovesBasic
{
	class DefaultDie;
	class ManActions
	{
		TKA_A3_UAZ_Gunner01 = "TKA_A3_UAZ_Gunner01";
		TKA_A3_UAZ_Gunner02 = "TKA_A3_UAZ_Gunner02";
		TKA_A3_SPG_Car_Gunner = "TKA_A3_SPG_Car_Gunner";
		TKA_A3_BRDM2_Driver = "TKA_A3_BRDM2_Driver";
		TKA_A3_BRDM2_Gunner = "TKA_A3_BRDM2_Gunner";
		TKA_A3_BRDM2_Cargo01 = "TKA_A3_BRDM2_Cargo01";
		TKA_A3_BRDM2_Cargo02 = "TKA_A3_BRDM2_Cargo02";
		TKA_A3_GAZ_Gunner = "TKA_A3_GAZ_Gunner";
		TKA_A3_LR_gunner03_EP1="TKA_A3_LR_gunner03_EP1";
	};
};
class CfgMovesMaleSdr: CfgMovesBasic
{
	class States
	{
		class TKA_A3_Stryker_Dead: DefaultDie
		{
			actions = "DeadActions";
			speed = 0.5;
			looped = 0;
			terminal = 1;
			file = "\tka_a3\tka_a3_vehicles\Anims\Stryker_Dead.rtm";
			connectTo[] = {"Unconscious",0.1};
		};
		class Crew;
		class TKA_A3_UAZ_Gunner01: Crew
		{
			file = "\tka_a3\tka_a3_vehicles\Anims\UAZ_Gunner01.rtm";
			interpolateTo[] = {"TKA_A3_Stryker_Dead",1};
		};
		class TKA_A3_UAZ_Gunner02: Crew
		{
			file = "\tka_a3\tka_a3_vehicles\Anims\UAZ_Gunner02.rtm";
			interpolateTo[] = {"TKA_A3_Stryker_Dead",1};
		};
		class TKA_A3_SPG_Car_Gunner: Crew
		{
			file = "\tka_a3\tka_a3_vehicles\Anims\UAZ_spg9_gunner.rtm";
			interpolateTo[] = {"TKA_A3_Stryker_Dead",1};
		};
		class TKA_A3_KIA_BRDM2_Driver: DefaultDie
		{
			actions = "DeadActions";
			speed = 0.5;
			looped = 0;
			terminal = 1;
			file = "\tka_a3\tka_a3_vehicles\Anims\KIA_BRDM2_Driver.rtm";
			connectTo[] = {"Unconscious",0.1};
		};
		class TKA_A3_BRDM2_Driver: Crew
		{
			file = "\tka_a3\tka_a3_vehicles\Anims\BRDM2_Driver.rtm";
			interpolateTo[] = {"TKA_A3_KIA_BRDM2_Driver",1};
		};
		class TKA_A3_KIA_BRDM2_Gunner: DefaultDie
		{
			actions = "DeadActions";
			speed = 0.5;
			looped = 0;
			terminal = 1;
			file = "\tka_a3\tka_a3_vehicles\Anims\KIA_BRDM2_Gunner.rtm";
			soundEnabled = 0;
			connectTo[] = {"Unconscious",0.1};
		};
		class TKA_A3_BRDM2_Gunner: Crew
		{
			file = "\tka_a3\tka_a3_vehicles\Anims\BRDM2_Gunner.rtm";
			interpolateTo[] = {"TKA_A3_KIA_BRDM2_Gunner",1};
		};
		class TKA_A3_KIA_BRDM2_Cargo01: DefaultDie
		{
			actions = "DeadActions";
			speed = 0.5;
			looped = 0;
			terminal = 1;
			file = "\tka_a3\tka_a3_vehicles\Anims\KIA_BRDM2_Cargo01.rtm";
			soundEnabled = 0;
			connectTo[] = {"Unconscious",0.1};
		};
		class TKA_A3_BRDM2_Cargo01: Crew
		{
			file = "\tka_a3\tka_a3_vehicles\Anims\BRDM2_Cargo01.rtm";
			interpolateTo[] = {"TKA_A3_KIA_BRDM2_Cargo01",1};
		};
		class TKA_A3_KIA_BRDM2_Cargo02: DefaultDie
		{
			actions = "DeadActions";
			speed = 0.5;
			looped = 0;
			terminal = 1;
			file = "\tka_a3\tka_a3_vehicles\Anims\KIA_BRDM2_Cargo02.rtm";
			soundEnabled = 0;
			connectTo[] = {"Unconscious",0.1};
		};
		class TKA_A3_BRDM2_Cargo02: Crew
		{
			file = "\tka_a3\tka_a3_vehicles\Anims\BRDM2_Cargo02.rtm";
			interpolateTo[] = {"TKA_A3_KIA_BRDM2_Cargo02",1};
		};
		class TKA_A3_KIA_GAZ_Gunner: DefaultDie
		{
			actions = "DeadActions";
			file = "\tka_a3\tka_a3_vehicles\Anims\KIA_GAZ_Gunner.rtm";
			speed = 0.5;
			looped = 0;
			terminal = 1;
			soundEnabled = 0;
			connectTo[] = {"Unconscious",0.1};
		};
		class TKA_A3_GAZ_Gunner: Crew
		{
			file = "\tka_a3\tka_a3_vehicles\Anims\Stryker_GunnerOut.rtm";
			interpolateTo[] = {"TKA_A3_KIA_GAZ_Gunner",1};
		};
		class TKA_A3_LR_gunner03_EP1: Crew
		{
			file="\tka_a3\tka_a3_vehicles\Anims\LR_gunner03";
			connectTo[]= {"TKA_A3_Stryker_Dead",1};
		};
	};
};
//};
