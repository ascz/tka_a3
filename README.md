Takistani Army A3 - [Official BI forum thread](https://forums.bistudio.com/topic/178264-takistani-army-a3-tka-a3/)
===========
This mod contains various infantry units, weapons and equipment. Weapons and vehicles are ports from Arma 2 and part of CUP / RHS.
Our goal is to include most of the content from the Arma 2 TKA faction in Arma 3 standards.

<br />
This project is open source and all the source can be found on GitHub - https://gitlab.com/ascz/tka_a3

*Note that this project is now discontinued.*


<br />
Dependencies:
===========
[@CBA_A3](https://github.com/CBATeam/CBA_A3/releases), [@CUP_Weapons](http://cup-arma3.org/downloads/cup-weapons), [@RHS_AFRF](www.rhsmods.org/mod/), [@RHS_USAF](www.rhsmods.org/mod/), [@RDS_Tank](http://forums.bistudio.com/showthread.php?174135-East-Static-Weapons-Pack)

<br />
Download
===========
*The mod can be found on these official release mirrors:*

[STEAM WORKSHOP](http://steamcommunity.com/sharedfiles/filedetails/?id=387819331)

[WITHSIX](http://withsix.com/p/Arma-3/mods/rQvZ_fX46EC1PyuL5sUHng/Takistani-Army-TKA-A3)

[ARMAHOLIC](http://www.armaholic.com/page.php?id=28120)


<br />
Donate
===========
If you like our work, please consider donating ;)

<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=evromalarkey%40gmail%2ecom&lc=US&item_name=ARMAseries%2ecz&currency_code=EUR&bn=PP%2dDonationsBF%3abtn_donateCC_LG%2egif%3aNonHosted"><img src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif"></a>

<br />
Credits
============
* EvroMalarkey - Project lead, Configs, Models porting
* Taurus - Textures, Configs
* Kllrt - Models porting, Configs

<br />
Additional Credits
============
* Bohemia Interactive - Arma2 Sample Models (ACR rvmats, textures and models)
* CUP team - For CUP Weapons
* RHS team - For RHS AFRF and A2 ports
* Reyhard - For sharing his A2 ports
* Toadie - For his outstanding HLC AK pack
